<?php
	error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    
    include('db_connections.php');
    include('queries.php');

    $dat_ini = explode('/',$_GET['date_ini']);
    $date_ini = $dat_ini[2].'-'.$dat_ini[1].'-'.$dat_ini[0];
    $dat_end = explode('/',$_GET['date_end']);
    $date_end = $dat_end[2].'-'.$dat_end[1].'-'.$dat_end[0];
    // $date_ini = '2018-10-22';
    // $date_end = '2018-11-22';
    // $cond = 'and month(created_at)=09 and year(created_at)=2018';
    // $cond_ms ='AND MONTH(U_GSP_CADATA)=9 AND YEAR(U_GSP_CADATA)=2018';

    $db_my = new db('mg','mage');
    $db_ms = new db();

    $pedidosmage = $db_my->make_query(queries::get_pedidos_magento(),[$date_ini,$date_end],PDO::FETCH_ASSOC);
    $pedidossap = $db_ms->make_query(queries::get_pedidos_sap(),[$date_ini,$date_end],PDO::FETCH_ASSOC);

    unset($db_my);
    unset($db_ms);

    $ordersmage = array();
    foreach($pedidosmage as $pedido) {
    	$ordersmage[]=$pedido;
    }

    $orderssap = array();
    foreach ($pedidossap as $pedido) {
    	$orderssap[]=$pedido;
    }

    $ordersfinal = array();
    $found= array();
    foreach ($ordersmage as $key =>$ordermage) {
    	$allforthis = array();
    	foreach ($orderssap as $key =>$ordersap) {
    		if ((strpos($ordersap['U_GSP_COMENT'],$ordermage['increment_id'])!==false) && $ordersap['U_GSP_CATOTA']>0) {
    			$allforthis = array_merge($ordermage,$ordersap);
    			$found=array_merge($found,array($key));
    			unset($orderssap[$key]);
    		}
    		# code...
    	}
    	if ($allforthis) $ordersfinal[] = $allforthis;
    	else $ordersfinal[] = $ordermage;
    	unset($allforthis);
    }

    $data = [];
    foreach($ordersfinal as $orderfinal) {
        $num_orders = $orderfinal['increment_id'] . ' / ' . @$orderfinal['U_GSP_CANUME'];
        $fecha_sap = isset($orderfinal['U_GSP_CADATA']) ? date('Y-m-d', strtotime($orderfinal['U_GSP_CADATA'])) : '****-**-**';
        $fechas = date('Y-m-d', strtotime($orderfinal['created_at'])) . ' / ' . $fecha_sap;
        $moneys = number_format((float)$orderfinal['base_grand_total'], 2, ',', '') . ' ' . $orderfinal['order_currency_code'] . ' / ' . @number_format((float)$orderfinal['U_GSP_CATOTA'], 2, ',', '');
        $tipo_pagos = $orderfinal['method'] . ' / ' . @$orderfinal['Descript'];
        if(isset($orderfinal['Descript']) && $orderfinal['method'] !== $orderfinal['Descript']) {
            $tipo_pagos = '<div style="display:none">1</div><font color="red"><b>' . $tipo_pagos . '</b></font>';
        }
        $web_coment = @$orderfinal['U_GSP_COMENT'];
        $client = @$orderfinal['U_GSP_CACLIE'];
        $data[] = ['num_ped' => $num_orders, 'web_coment' => $web_coment, 'fecha' => $fechas, 'money' => $moneys, 'pago' => $tipo_pagos, 'cliente' =>$client];
    }
   
    $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data
    );

    echo json_encode($results);
    // var_dump($ordersfinal);

    // var_dump($orderssap);

?>