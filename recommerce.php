<?php
include('header.php');
?>

    <div class="contenedor">
        
        Entre
        <input id="date_ini" class="datepicker" class="form-control" name="date_sellout" value="<?php echo date ('d/m/Y', strtotime ( '-1 month', strtotime(date('Y-m-j')))); ?>" readonly="readonly" type="text">
        Y <input id="date_end" class="datepicker" class="form-control" name="date_sellout" value="<?php echo date('d/m/Y'); ?>" readonly="readonly" type="text">
        <button id="search" onclick="reload_table()" class="btn btn-primary">Buscar</button>

        <table id="data-info-1" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th># MG / SAP</th>
                    <th>Comentario</th>
                    <th>Fecha MG / SAP</th>
                    <th>€ MG / SAP</th>
                    <th>Forma de Pago</th>
                    <th># Cliente</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th># MG / SAP</th>
                    <th>Comentario</th>
                    <th>Fecha MG / SAP</th>
                    <th>€ MG / SAP</th>
                    <th>Forma de Pago</th>
                    <th># Cliente</th>
                </tr>
            </tfoot>
        </table>

        <table id="data-info-2" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th># MG / SAP</th>
                    <th>Comentario</th>
                    <th>Fecha MG / SAP</th>
                    <th>€ MG / SAP</th>
                    <th>Forma de Pago</th>
                    <th># Cliente</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th># MG / SAP</th>
                    <th>Comentario</th>
                    <th>Fecha MG / SAP</th>
                    <th>€ MG / SAP</th>
                    <th>Forma de Pago</th>
                    <th># Cliente</th>
                </tr>
            </tfoot>
        </table>
    </div>

<script type="text/javascript" language="javascript" class="init">

$(".datepicker").datepicker(
		    {viewMode: 'years',
		     format: 'mm-yyyy',
         minViewMode: "months"
		});
</script>